# How to install wfview

### 1. Install prerequisites:
(Note, some packages may have slightly different version numbers, this should be ok for minor differences.)
~~~
sudo apt-get install build-essential
sudo apt-get install qt5-qmake
sudo apt-get install qt5-default
sudo apt-get install libqt5core5a
sudo apt-get install qtbase5-dev
sudo apt-get install libqt5serialport5 libqt5serialport5-dev
sudo apt-get install libqt5multimedia5
sudo apt-get install libqt5multimedia5-plugins
sudo apt-get install qtmultimedia5-dev
sudo apt-get install git 
~~~
Now you need to install qcustomplot. There are two versions that are commonly found in linux distros: 1.3 and 2.0. Either will work fine. If you are not sure which version your linux install comes with, simply run both commands. One will work and the other will fail, and that's fine!

qcustomplot1.3 for older linux versions (Linux Mint 19.x, Ubuntu 18.04): 

~~~
sudo apt-get install libqcustomplot1.3 libqcustomplot-doc libqcustomplot-dev
~~~

qcustomplot2 for newer linux versions (Linux Mint 20, Ubuntu 19, Rasbian V?, Debian 10):

~~~
sudo apt-get install libqcustomplot2.0 libqcustomplot-doc libqcustomplot-dev
~~~

optional for those that want to work on the code using the QT Creator IDE: 
~~~
sudo apt-get install qtcreator qtcreator-doc
~~~

### 2. Clone wfview to a local directory on your computer:
~~~ 
cd ~/Documents
git clone https://gitlab.com/eliggett/wfview.git
~~~

### 3. Create a build directory, compile, and install:
~~~
mkdir build
cd build
qmake ../wfview/wfview.pro
make -j
sudo ./install.sh
~~~


### 4. You can now launch wfview, either from the terminal or from your desktop environment. If you encounter issues using the serial port, run the following command: 
~~~
sudo chown $USER /dev/ttyUSB*
~~~

- most linux systems just need to have you added to the dialout group as that's persistent and more secure.

sudo usermod -aG dialout $USER 
(don't forget to log out and log in)


### opensuse install ###
---

the following is a raw - only one time tested piece of information 

install suse 15.x  (did this on a kde virtual machine leap 15.2)

qt5:

wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run

- chmod +x qt-unified-linux-x64-online.run
- sudo zypper in --type pattern devel_basis
- kdesu ./qt-unified-linux-x64-online.run
- install base qt5 default qt5 desktop development

- sudo zypper in libQt5Widgets-devel libqt5-qtbase-common-devel libqt5-qtserialport-devel libQt5SerialPort5 qcustomplot-devel libqcustomplot2 libQt5PrintSupport-devel 

- mkdir src
- cd src
- git clone https://gitlab.com/eliggett/wfview.git
- cd ../
- mkdir build 
- cd build
- qmake-qt5 ../src/wfview/wvfiew.pro
- make -j
- sudo ./install.sh


---
